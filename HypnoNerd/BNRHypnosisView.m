//
//  BNRHypnosisView.m
//  Hypnosister
//
//  Created by Jay Liew on 6/8/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import "BNRHypnosisView.h"

@interface BNRHypnosisView()

@property (strong, nonatomic) UIColor *circleColor;

@property (strong, nonatomic) UISegmentedControl *segmentedControl;

@end


@implementation BNRHypnosisView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // All BNRHypnosisViews start with a clear background color
        self.backgroundColor = [UIColor clearColor];
        self.circleColor = [UIColor lightGrayColor];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    CGRect bounds = self.bounds;
    
    CGPoint center;
    center.x = bounds.origin.x + bounds.size.width / 2.0;
    center.y = bounds.origin.y + bounds.size.height / 2.0;

    //float radius = MIN(bounds.size.width, bounds.size.height) / 2.0;
    float maxRadius = hypot(bounds.size.width, bounds.size.height) / 2.0;
    
    UIBezierPath *path = [[UIBezierPath alloc] init];

    for (float currentRadius = maxRadius; currentRadius > 0; currentRadius -= 20) {
        
        [path moveToPoint:CGPointMake(center.x + currentRadius, center.y)];
        
        [path addArcWithCenter:center
                        radius:currentRadius
                    startAngle:0.0
                      endAngle:M_PI * 2.0
                     clockwise:YES];
    }
     
    
    path.lineWidth = 10;
    
    [self.circleColor setStroke];
    
    [path stroke];

    if(!self.segmentedControl){
        _segmentedControl = [[UISegmentedControl alloc] initWithItems:@[@"Red", @"Green", @"Blue"]];
        
        CGPoint segmentControlOrigin;
        CGFloat windowHeight = [self window_height] *2;
        CGFloat windowWidth = [self window_width] *2 ;
        CGFloat segmentControlWidth = 200.0;
        CGFloat segmentControlHeight = 50.0;
        
        segmentControlOrigin.x = (windowWidth - segmentControlWidth) / 2;
        segmentControlOrigin.y = (windowHeight - segmentControlHeight) / 3.0 + 70;
        
        _segmentedControl.frame = CGRectMake(segmentControlOrigin.x, segmentControlOrigin.y, segmentControlWidth, segmentControlHeight);
        
        [_segmentedControl addTarget:self action:@selector(changeColor:) forControlEvents:UIControlEventValueChanged];
        
        [self addSubview:_segmentedControl];

    }
}

- (void)changeColor:(id)sender{
    UISegmentedControl *uisc = (UISegmentedControl *)sender;
    
    switch (uisc.selectedSegmentIndex) {
        case 0:
            self.circleColor = [UIColor colorWithRed:100.0
                                               green:0.0
                                                blue:0.0
                                               alpha:1.0];
            break;

        case 1:
            self.circleColor = [UIColor colorWithRed:0.0
                                               green:100.0
                                                blue:0.0
                                               alpha:1.0];
            break;

        case 2:
            self.circleColor = [UIColor colorWithRed:0.0
                                               green:0.0
                                                blue:100.0
                                               alpha:1.0];
            break;
            
//        default:
//            break;
    }
    
}


- (CGFloat)window_height {
    return [UIScreen mainScreen].applicationFrame.size.height;
}

- (CGFloat)window_width {
    return [UIScreen mainScreen].applicationFrame.size.width;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSLog(@"%@ was touched", self);
    
    float red = (arc4random() % 100) / 100.0;
    float green = (arc4random() % 100) / 100.0;
    float blue = (arc4random() % 100) / 100.0;
    
    UIColor *randomColor = [UIColor colorWithRed:red
                                           green:green
                                            blue:blue
                                           alpha:1.0];
    self.circleColor = randomColor;
}

- (void)setCircleColor:(UIColor *)circleColor
{
    _circleColor = circleColor;
    [self setNeedsDisplay];
}

@end
