//
//  BNRHypnosisViewController.m
//  HypnoNerd
//
//  Created by Jay Liew on 6/21/15.
//  Copyright (c) 2015 Jay Liew. All rights reserved.
//

#import "BNRHypnosisViewController.h"
#import "BNRHypnosisView.h"

@interface BNRHypnosisViewController () <UITextFieldDelegate, UIScrollViewDelegate>

@property (strong, nonatomic) BNRHypnosisView *backgroundView;
@property (strong, nonatomic) UIScrollView *scrollView;

@end

@implementation BNRHypnosisViewController

- (instancetype)initWithNibName:(NSString *)nibNameOrNil
                         bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];

    if(self){
        self.tabBarItem.title = @"Hypnotize";
        
        UIImage *image = [UIImage imageNamed:@"Hypno.png"];
        
        self.tabBarItem.image = image;
    }
    return self;
}

- (void)loadView
{
    CGRect frame = [UIScreen mainScreen].bounds;
    CGRect biggerFrame = frame;
    biggerFrame.size.width *= 2;
    biggerFrame.size.height *= 2;
    
    self.scrollView = [[UIScrollView alloc] initWithFrame:frame];
    self.scrollView.contentSize = CGSizeMake(frame.size.width*2, frame.size.height*2);
  
    // This is to center the concentric circles
    self.scrollView.contentOffset = CGPointMake(biggerFrame.size.width/4, biggerFrame.size.height/4);
    
    self.backgroundView = [[BNRHypnosisView alloc] initWithFrame:biggerFrame];
    
    // center the textfield
    CGRect textFieldRect = CGRectMake(((biggerFrame.size.width-240)/2), ((biggerFrame.size.height-30)/3), 240, 30);

    UITextField *textField = [[UITextField alloc] initWithFrame:textFieldRect];
    
    textField.borderStyle = UITextBorderStyleRoundedRect;
    textField.placeholder = @"Hypotize me";
    textField.returnKeyType = UIReturnKeyDone;
    
    textField.delegate = self;

    self.scrollView.minimumZoomScale = 6.0;
    self.scrollView.maximumZoomScale = 1.0;
    self.scrollView.multipleTouchEnabled = YES;
    self.scrollView.delegate = self;

    [self.backgroundView addSubview:textField];
    
    [self.scrollView addSubview:self.backgroundView];
    
    self.view = self.scrollView;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self drawHypnoticMessage:textField.text];
    textField.text = @"";
    [textField resignFirstResponder];
    return YES;
}

- (void)drawHypnoticMessage:(NSString *)message
{
    CGRect biggerFrame = [UIScreen mainScreen].bounds;
    biggerFrame.size.width *= 2;
    biggerFrame.size.height *= 2;

    for (int i = 0; i < 50; i++){
        UILabel *messageLabel = [[UILabel alloc] init];
        
        messageLabel.backgroundColor = [UIColor clearColor];
        messageLabel.textColor = [UIColor blackColor];
        messageLabel.text = message;
        
        [messageLabel sizeToFit];
        
        int width = biggerFrame.size.width - messageLabel.bounds.size.width;
        int x = arc4random() % width;
        
        int height = biggerFrame.size.height - messageLabel.bounds.size.height;
        int y = arc4random() % height;
        
        CGRect frame = messageLabel.frame;
        frame.origin = CGPointMake(x, y);
        messageLabel.frame = frame;
        
        [self.view addSubview:messageLabel];
        
        UIInterpolatingMotionEffect *motionEffect;
        motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.x" type:UIInterpolatingMotionEffectTypeTiltAlongHorizontalAxis];
        motionEffect.minimumRelativeValue = @-25;
        motionEffect.maximumRelativeValue = @25;
        [messageLabel addMotionEffect:motionEffect];

        motionEffect = [[UIInterpolatingMotionEffect alloc] initWithKeyPath:@"center.y" type:UIInterpolatingMotionEffectTypeTiltAlongVerticalAxis];
        motionEffect.minimumRelativeValue = @-25;
        motionEffect.maximumRelativeValue = @25;
        [messageLabel addMotionEffect:motionEffect];

    }
}


-(UIView *) viewForZoomingInScrollView:(UIScrollView *)scrollView
{
    return self.backgroundView;
}

@end
